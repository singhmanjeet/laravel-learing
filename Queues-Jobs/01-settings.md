# Queque

Queues in Laravel manage and execute tasks asynchronously, allowing the system to handle processes in the background without blocking the main application

## Env Setup

To setup .env Queue connection.

```
QUEUE_CONNECTION=database
```

## create Queue Table

```
php artisan queue:table
php artisan migrate
```

## create job
It creates File inside [app/Jobs/QueueSendMailJob.php].
```
php artisan make:job QueueSendMailJob
```
## Create Route

```
Route::get('queue', [SendingMailableController::class, 'index']);
```

## Create Controller to handle Route action.

```
php artisan make:controller SendingMailableController
```

## Controller Setting

```
namespace App\Http\Controllers;

use App\Jobs\QueueSendMailJob;
use Illuminate\Http\Request;

class SendingMailableController extends Controller
{
    public function index()
    {
        $data = ['title' => 'Mail Title', 'content' => 'Mail Content'];
        dispatch(new QueueSendMailJob($data));
    }
}

```

## Setup Job file

```
<?php

namespace App\Jobs;

use App\Mail\QueueSendEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class QueueSendMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::to('manjeet@codeflies.com')->send(new QueueSendEmail($this->data));
    }
}

```

## Create Mailable File to send mail
It creates Mailable File inside [app/Mail/QueueSendEmail.php]
```
php artisan make:mail QueueSendEmail
```

## Setup Mailable file


```
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class QueueSendEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Queue Send Email',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'mailer.mail',
            with: $this->data
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}

```


## Run work command

```
php artisan queue:work
```