# Model Event by protected $displatchesEvents

## Model Events List.
retrieved : When record or records retrieved

creating : Before record create

created : After record created

updating : Before record update eg. this method we can use when any image update 

updated : After record updated

saving : Before record save

saved  : after record saved

deleting : before record delete eg. this method we can use when any image delete 

deleted  : after record deleted

restoring : before restore record

restored : after restored record

## create controller

```
php artisan make:controller CrudController
```

## create model with migration.

```
php artisan make:model Crud -m
```

## create column using crud migration file.

```
Schema::create('cruds', function (Blueprint $table) {
            $table->id();
            $table->string('name');  // for name
            $table->string('content'); // for content
            $table->timestamps();
        });
```

## create routes for crud.

```
Route::get('/', [CrudController::class, 'index']);
Route::get('/create', [CrudController::class, 'create']);
Route::get('/update/{id}', [CrudController::class, 'update']);
Route::get('/delete/{id}', [CrudController::class, 'destroy']);

```


## create controller methods for crud operations.

```
namespace App\Http\Controllers;

use App\Models\Crud;
use Illuminate\Http\Request;

class CrudController extends Controller
{
    public function index()
    {
        return Crud::all();
    }
    public function create()
    {
        $obj = new Crud();
        $obj->name = "Manjeet";
        $obj->content = "Manjeet " . "content";
        $obj->save();
    }
    public function update($id)
    {
        $obj = Crud::find($id);
        $obj->name = "Manjeet";
        $obj->content = "Manjeet singh " . "content";
        $obj->save();
    }

    public function destroy($id)
    {
        Crud::find($id)->delete();
    }
}

```


## creating Events for creating and created events.
Note : Naming convention is not nessessary, But it make your event more understandable.
Naming Conventions : ModelName+EventName+Event example: CrudCreatingEvent,CrudCreatedEvent

```
php artisan make:event CrudCreatingEvent   // for creating event
php artisan make:event CrudCreatedEvent    // for created event
```

## creating Listeners for creating and created events and attach realated event with listener.

Note : Naming convention is not nessessary, But it make your event more understandable.
Naming Conventions : ModelName+EventName+Listener example: CrudCreatingListener,CrudCreatedListener

```
php artisan make:listener CrudCreatingListener --event=CrudCreatingEvent  // for creating listener
php artisan make:listener CrudCreatedListener --event=CrudCreatedEvent    // for created listener
```


## Register event and listeners in EventServiceProvider.php

```

    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        CrudCreatingEvent::class => [
            CrudCreatingListener::class
        ],
        CrudCreatedEvent::class => [
            CrudCreatedListener::class
        ]
    ];

```

## Bind event with event class in model as Crud.

```
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Crud extends Model
{
    use HasFactory;

    protected $dispatchesEvents = [
        'creating' => \App\Events\CrudCreatingEvent::class,  // Binding creating event
        'created' => \App\Events\CrudCreatedEvent::class     // Binding created event
    ];
}

```

## Setting variable in CrudCreatingEvent class.


```
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

```


## same as Setting variable in CrudCreatedEvent class.

```
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

```


## handling event action by listener for CrudCreatingEvent.

```
namespace App\Listeners;

use App\Events\CrudCreatingEvent;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CrudCreatingListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(CrudCreatingEvent $event): void
    {
        Log::info('Creating Event has fired' . json_encode($event->data));
    }
}

```


## handling event action by listener for CrudCreatedEvent.