# Notification
Notification is a process that is used to send notification to user via multiple chanels such as mail,database,sms chanel.

## To create a notification via terminal
By this command, A notification class will be created inside app\notifications folder.

```
php artisan make:notification InvoicePaid
```

# Ways of Sending Notifications
There are two ways to send notifications.

## Using the Notifiable Trait

By this, I must need to use Notifiable trail inside User Model.



```
<?php
 
namespace App\Models;
 
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
 
class User extends Authenticatable
{
    use Notifiable;
}
```
and then I can send notification calling given method.

```
use App\Notifications\InvoicePaid;
$user=User::find(1); 
$user->notify(new InvoicePaid($invoice));
```



## Using the Notification Facade
In this case , there is not need to use trail inside User model. We can directly use Notification Facade.

Note: This approach is useful when you need to send a notification to multiple notifiable entities such as a collection of users. 

```
use Illuminate\Support\Facades\Notification;
$users= User::all(); 
Notification::send($users, new InvoicePaid($invoice));

```
OR 

You can also send notifications immediately using the sendNow method. This method will send the notification immediately even if the notification implements the ShouldQueue interface.

```
use Illuminate\Support\Facades\Notification;
$users= User::all(); 
Notification::sendNow($users, new InvoicePaid($invoice));
```