# Sending Mail By Mailable

## Create Mailable

```
php artisan make:mail TestMail
```
This command create mailable template file inside App\Mail as TestMail.php

## Create Route

```
Route::get('/send', [PdfController::class, 'send']);
```

## Create Controller

```
php artisan make:controller PdfController
```

## create methods in PdfController as send() 

```
namespace App\Http\Controllers;

use App\Mail\TestMail;
use Illuminate\Http\Request;
use PDF;
use Mail;

class PdfController extends Controller
{
    public function send()
    {
        $data['email'] = "manjeet@codeflies.com";
        $data['title'] = "This is my title";
        $data['body'] = "this is my content";
        $data['subject'] = "This is my test subject";
        $pdf = PDF::loadView('mail.xyz', $data);
        $data['pdf'] = $pdf;
        Mail::to($data['email'])->send(new TestMail($data));
        dd("mail sent");
    }
}


```

## install dom-pdf for Send PDF Attachment

```
composer require barryvdh/laravel-dompdf

```

## Setting Mailable as TestMail

```
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Attachment;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class TestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public $mailData; // create public variable
    public function __construct($mailData)
    {
        $this->mailData = $mailData; // store value in public variable
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: $this->mailData['subject'], // set mail subject
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'mail.xyz', // for Binding view template
            with: $this->mailData // passing data to the view
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [
            Attachment::fromData(fn () => $this->mailData['pdf']->output(), 'report.pdf')->withMime('application/pdf') // For attachment
        ];
    }
}


```


## create template 
mail.xyz.blade.php

```
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$title}}</title>
</head>

<body>
    <h1>{{$title}}</h1>
    <p>{{ $body}}</p>

</body>

</html>

```