# Getting Started with Laravel Model Events
Laravel Model events allow you to tap into various points in a model’s lifecycle, and can even prevent a save or delete from happening.

## Life Cycle Methods of Model Event .

retrieved : When record or records retrieved

creating : Before record create

created : After record created

updating : Before record update eg. this method we can use when any image update 

updated : After record updated

saving : Before record save

saved  : after record saved

deleting : before record delete eg. this method we can use when any image delete 

deleted  : after record deleted

restoring : before restore record

restored : after restored record

## create controller

```
php artisan make:controller CrudController
```

## create model with migration.

```
php artisan make:model Crud -m
```

## create column using crud migration file.

```
Schema::create('cruds', function (Blueprint $table) {
            $table->id();
            $table->string('name');  // for name
            $table->string('content'); // for content
            $table->timestamps();
        });
```

## create routes for crud.

```
Route::get('/', [CrudController::class, 'index']);
Route::get('/create', [CrudController::class, 'create']);
Route::get('/update/{id}', [CrudController::class, 'update']);
Route::get('/delete/{id}', [CrudController::class, 'destroy']);

```


## create controller methods for crud operations.

```
namespace App\Http\Controllers;

use App\Models\Crud;
use Illuminate\Http\Request;

class CrudController extends Controller
{
    public function index()
    {
        return Crud::all();
    }
    public function create()
    {
        $obj = new Crud();
        $obj->name = "Manjeet";
        $obj->content = "Manjeet " . "content";
        $obj->save();
    }
    public function update($id)
    {
        $obj = Crud::find($id);
        $obj->name = "Manjeet";
        $obj->content = "Manjeet singh " . "content";
        $obj->save();
    }

    public function destroy($id)
    {
        Crud::find($id)->delete();
    }
}

```

## Now its time to create model event lifecycle method.
in Crud.php model
```
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Crud extends Model
{
    use HasFactory;

    public static function boot()
    {
        parent::boot();
        static::retrieved(function ($item) {
            Log::info("This is my retrieved lifecycle method." . json_encode($item));
        });
        static::creating(function ($item) {
            Log::info("This is my creating lifecycle method." . json_encode($item));
        });
        static::created(function ($item) {
            Log::info("This is my created lifecycle method." . json_encode($item));
        });
        static::saving(function ($item) {
            Log::info("This is my saving lifecycle method." . json_encode($item));
        });
        static::saved(function ($item) {
            Log::info("This is my saved lifecycle method." . json_encode($item));
        });
        static::updating(function ($item) {
            Log::info("This is my updating lifecycle method." . json_encode($item));
        });
        static::updated(function ($item) {
            Log::info("This is my updated lifecycle method." . json_encode($item));
        });
        static::deleting(function ($item) {
            Log::info("This is my deleting lifecycle method." . json_encode($item));
        });
        static::deleted(function ($item) {
            Log::info("This is my deleted lifecycle method." . json_encode($item));
        });
    }
}

```




