# Sending Notifications Via mail Chanel

## create Notification

```
php artisan make:notification EmailNotification
```
By using this command , a EmailNotification class will be created inside app\Notifications folder.

## set chanel

inside via method, I need to pass 'mail' chanel as array.

```
 public function via(object $notifiable): array
{
    return ['mail'];
}

```
## change content and link

```
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->line('This is my first Email')  // chnage this content
            ->action('My Action', url('/my-action')) // button name and link
            ->line('Thank you for using our application!'); // change this content
    }
```

## Passing User information

```
    public $user;
    public function __construct($user)
    {
        $this->user = $user;
    }
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->line('This is my first Email' . $this->user['name']) // here user name has dynamic
            ->action('My Link', url('/my-link'))
           ->line('Thank you for using our application!');
    }
```

## create controller

```
php artisan make:controller EmailController
```

## Set Route

```
Route::get('/', [EmailController::class, 'index']);
```

## First Way : Using the Notifiable Trait
By this, I must need to use Notifiable trail inside User Model as 

Note: By this way, you can send one notification at a time or you need to use loop to send multiple notification at a time.
```
<?php
 
namespace App\Models;
 
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
 
class User extends Authenticatable
{
    use Notifiable;
}
```

and the Controller method as

```
<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Notifications\EmailNotification;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function index()
    {
        $user = User::find(1);
        $user->notify(new EmailNotification($user));
    }
}

```


## Second Way : Using the Notification Facade

In this case , there is not need to use trail inside User model. We can directly use Notification Facade.

Note: This approach is useful when you need to send a notification to multiple notifiable entities such as a collection of users. 

Note: Notifiaction without passing any value like invoice data.

```
<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Notifications\EmailNotification;
use Illuminate\Support\Facades\Notification;

class EmailController extends Controller
{
    public function index()
    {
        $users = User::all();
        Notification::send($users, new EmailNotification);
    }
}
```

Note: Notification with invoice Data

First setup EmailNotification class as :

```
public $invoice;


public function __construct($invoice)
{
    $this->invoice = $invoice;
}


public function toMail(object $notifiable): MailMessage
{
    return (new MailMessage)
        ->line('Invoice ID: ' . $this->invoice['InvoiceId'])
        ->line('Item: ' . $this->invoice['Item'])
        ->line('Price: ' . $this->invoice['Price'])
        ->action('My Link', url('/invoice/' . $this->invoice['InvoiceId']))
        ->line('Thank you for using our application!');
}

```

and Now seup Conroller method as:

```
<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Notifications\EmailNotification;
use Illuminate\Support\Facades\Notification;

class EmailController extends Controller
{
    public function index()
    {
        $users = User::all();
        $invoice = [
            "InvoiceId" => "12345",
            "Item" => "First Items",
            "Price" => 120
        ];
        Notification::send($users, new EmailNotification($invoice));
    }
}

```



# Send Notification via Custom Blade Template

To do this, I need to call view method and set blade file and also you can pass data as well.

```
public function toMail(object $notifiable): MailMessage
{
    return (new MailMessage)->view('welcome');
}
```