# Event Example To Send Custom Email


## create controller.
It will be created in side App\Http\Controllers

```
php artisan make:controller TestController
```

## create event

```
php artisan make:event SendEvent
```
SendEvent.php file will be added inside App\Events.

## create listener and attach SendEvent event with listener.

```
php artisan make:listener SendFired --event=SendEvent
```
SendFired.php will be added inside App\Listeners.

## create blade templete to attach with mail.
email.blade.php file inside resoures/mailer/email.blade.php

```
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Title</title>
</head>

<body>
    <h1>{{ $title}}</h1>
    <p>{{ $content}}</p>
</body>

</html>

```



## create controller method.
There are many ways to dispatch custom event.

By using event() helper
```
namespace App\Http\Controllers;

use Mail;
use App\Events\SendEvent;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function send()
    {
        $data = ['title' => 'Sample Mailer', 'content' => 'This is my sample content'];
        event(new SendEvent($data)); // to trigger custom event where SendEvent is a Event that will be fired

    }
}

```
By using SendEvent::dispatch() method.
```
namespace App\Http\Controllers;

use Mail;
use App\Events\SendEvent;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function send()
    {
        $data = ['title' => 'Sample Mailer', 'content' => 'This is my sample content'];
        SendEvent::dispatch($data); to trigger custom event where SendEvent is a Event that will be fired

    }
}

```

## create route 

```
Route::get('/', [TestController::class, 'send']);
```

## get controller data inside event

change in App\Events\SendEvent.php

```
    public $data;   // This data variable must be public
    public function __construct($data)  // received from controller
    {
        //
        $this->data = $data;
    }

```

##  Changes inside Listener

Changes in handle method of App\Listeners\SendFired.php 

```
 public function handle(SendEvent $event): void
    {
        $data = $event->data;
        Mail::send('mailer.email', $data, function ($message) use ($data) {
            $message->to("manjeet@codeflies.com")->subject("this is testing event");
        });
    }
```

## Register event and listeners in EventServiceProvider.php

```
  protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SendEvent::class => [  // Event
            SendFired::class   // Listener
        ]
    ];
```