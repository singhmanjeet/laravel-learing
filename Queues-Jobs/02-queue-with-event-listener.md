# Event Listener with Queue

## Env Setup

To setup .env Queue connection.

```
QUEUE_CONNECTION=database
```

## create Queue Table

```
php artisan queue:table
php artisan migrate
```

## Create Route

```
Route::get('queue', [SendingMailableController::class, 'index']);
```

## Create Controller to handle Route action.

```
php artisan make:controller SendingMailableController
```

## create Event
It creates Event File inside [app/Events/QueueEvent.php]
```
php artisan make:event QueueEvent
```

## Create Listener

It creates Listener File inside [app/Listeners/QueueListener.php]

```
php artisan make:listener QueueListener --event=QueueEvent
```


## Register Event with Listener in providers/EventServiceProvider.php

```
 protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        QueueEvent::class => [
            QueueListener::class
        ]
    ];

```


## Setup Controller

```
namespace App\Http\Controllers;

use App\Events\QueueEvent;
use Illuminate\Http\Request;

class SendingMailableController extends Controller
{
    public function index()
    {
        $data = ['title' => 'Mail Title', 'content' => 'Mail Content'];
        event(new QueueEvent($data));
    }
}

```

## Setup Event 

```

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class QueueEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     */
    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new PrivateChannel('channel-name'),
        ];
    }
}

```

## Setup Listener
To Achive the queue functionality with listener , I need to implement ShouldQueue  in listener class
```

namespace App\Listeners;

use App\Events\QueueEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class QueueListener implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(QueueEvent $event): void
    {
        $data = $event->data;
        Mail::send('mailer.mail', $data, function ($message) use ($data) {
            $message->to('manjeet@codeflies.com')->subject('This is my subject');
        });
    }
}

```

## Run Queue:work command

```
php artisan queue:work
```