# Firstly create new laravel project 

```
composer create-project laravel/laravel:^10.0 contact
```

# Go To contact folder

```
cd contact
```

# Now create directories on root directory of project

2 folder are needed.

```
package/contact
```
Where contact is a main folder for our package.

# Now Go TO contact directory and open new terminal to develop package.
by this command , you can open vccode editor for contact package.
```
code .
```

# Inside contact directory

## create composer.json file.

```
composer init
```
After that I need to fill some instructions as: where trick is a vendor name and contact is package name.

```
Package name (<vendor>/<name>) [codeflies/contact]: trick/contact
```
After that
```
Description []: This will store contact data to database.
```
and then enter n to skip.
```
Author [Manjeet Singh <ms8776776@gmail.com>, n to skip]: 
```
and then I need to set the stability.

For More information:  Available options (in order of stability) are dev, alpha, beta, RC, and stable. in composer.

So Currenly we are in development stage that why I need set minimum stability as dev.

```
Minimum Stability []: dev
```
and then I need to define package type as library.

```
Package Type (e.g. library, project, metapackage, composer-plugin) []: library
```
and then I need to set License as MIT. This license is a standard.

```
License []: MIT
```
and then I need to insure that you need any dependent package. Currenly no need to skip by entering no.

```
Would you like to define your dependencies (require) interactively [yes]? no
Would you like to define your dev dependencies (require-dev) interactively [yes]? no
```

and then 

```
Add PSR-4 autoload mapping? Maps namespace "Trick\Contact" to the entered relative path. [src/, n to skip]:
```
and press enter.

and finally press enter by verifying.

```
{
    "name": "trick/contact",
    "description": "This will store contact data to database.",
    "type": "library",
    "license": "MIT",
    "autoload": {
        "psr-4": {
            "Trick\\Contact\\": "src/"
        }
    },
    "minimum-stability": "dev",
    "require": {}
}

Do you confirm generation [yes]? yes
```

Now a composer.json file will be created with src/,vendor folders.

## Now Next 

Every package must be need an service provider inside src/ folder.

so I need to create ContactServiceProvider.php file and this service privider must extend a laravel service provide and also need to set namespace as well.

this file must have 2 methods as register() and boot().

```
<?php

namespace Trick\Contact;

use Illuminate\Support\ServiceProvider;

class ContactServiceProvider extends ServiceProvider
{
    public function register(){

    }
    public function boot(){
        
    }
}

```

Note: Boot method load view files and register method loads service means classes.

Note: namespace must be mapped with autoload psr-4. in my case it is added by default in composer.json file.

```
    "autoload": {
        "psr-4": {
            "Trick\\Contact\\": "src/"
        }
    },

```

# Now In development mode, how to connect our package with the laravel main project.

To do that, I need to open main project composer.json and set the actual package path which you are creating as:
add given line inside autoload -> psr-4 as 

```
"Trick\\Contact\\": "package/contact/src/"
```

example: 

```
 "autoload": {
        "psr-4": {
            "App\\": "app/",
            "Database\\Factories\\": "database/factories/",
            "Database\\Seeders\\": "database/seeders/",
            "Trick\\Contact\\": "package/contact/src/"  //this one line
        }
    },
```

After that Run this command to mapping package as:

```
composer dump-autoload
```



# Now I need to go inside package/contact/src folder and open ContactServiceProvider.php service provider

after open service provider, I need to create route for our package.

## To create Route, Follow given instructions.
1-> create routes/ folder inside src/ folder and inside routes/ folder create web.php file as 

```
package/contact/src/routes/web.php
``` 

2->  Now for testing purpose, I need to write simple route as:

```
Route::get('/contact', function () {
    return "<h1>This route is working fine.</h1>";
});
```

3-> Now I need to load route inside boot() method of created service provide (ContactServiceProvider) as :

```
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
    }
```

4->  Now I need to register our service provider (ContactServiceProvider) in main project config/app.php inside providers key as:
Trick\Contact\ContactServiceProvider::class

```
    'providers' => ServiceProvider::defaultProviders()->merge([
        
        Trick\Contact\ContactServiceProvider::class  // this one only
    ])->toArray(),
```



## Now Load view , To load views in package, I need to follow some instructions

1-> I need to create directories for view as :

```
package/contact/src/resources/views
```

2-> load view directory, in service provider.

```
    public function boot()
    {
       $this->loadViewsFrom(__DIR__ . '/resources/views', 'contact'); // where first parameter is pathof directory and second parameter is name of package.
    }

```

3-> Now I need to create a blade file in views folder as contact-us.blade.php inside package/contact/src/resources/views as write some content as :

```
<h1>Welcome to contact blade file</h1>
```

4-> Now I need to load view file inside route closer or controller as 

Note:  whenever I try to load view using view() helper, I need to use package_name::blade_file_name eg: contact::contact-us

```
<?php

use Illuminate\Http\Request;
use Trick\Contact\Http\Controllers\ContactController;

Route::group(['middleware' => 'web'], function () {
    Route::get('/contact', [ContactController::class, 'index'])->name('contact');
    Route::post('contact', [ContactController::class, 'store']); 
});
```


## To create controller,follow given steps.

1-> create Http/Controllers folder inside package/contact/src/ folder as :

```
package/contact/src/Http/Controllers
```
2-> create controller file as: ContactController.php

```
package/contact/src/Http/Controllers/ContactController.php 
```

3-> the main skeleton of controller file is:

```
<?php

namespace Trick\Contact\Http\Controllers;  // this is package namespace

use App\Http\Controllers\Controller;  // it is for extending controller
use Illuminate\Http\Request;  // to handle request

class ContactController extends Controller
{
    //
}


```



## To create model and migration,follow given steps

1-> First create model with migration as:

```
php artisan make:model Contact -m
```
2-> create model folder inside package/contact/src/  and drag Contact model from root model to package model, after that change only namespace as :

```
<?php

namespace Trick\Contact\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;
}

```

3-> same as create folder for migrations as database/migrations inside package/contact/src/, after that drag contact migration from root migration file to package migrations folder and then add required columns.

```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('query');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contacts');
    }
};

```

## To proceed next step, I need to create database and setup in root .env file as:

```
DB_DATABASE=lara_pkg
DB_USERNAME=root
DB_PASSWORD="Admin@#123"
```

## Now Load migration file as:
To load migration file, I need to add given method in ContactServiceProvide's boot() method as:

```
public function boot()
{
    $this->loadMigrationsFrom(__DIR__.'/database/migrations');
}
```

## Now I need to run migration command as:

```
php artisan migrate
```


## Now write code to store data in database using controller and model in package as:

```
<?php

namespace Trick\Contact\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Trick\Contact\Models\Contact;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact::contact-us');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'query' => 'required'
        ]);
        // Contact::create(['name' => 'Test', 'query' => 'test con']);
        $contact = new Contact();
        $contact->name = $request->post('name');
        $contact->query = $request->post('query');
        $contact->save();
        return redirect(route('contact'))->with(['message' => 'Information has been stored successfully..']);
    }
}

```

# Set up packagist account
## Firstly create account on packgist plateform and verify account via mail.
## Now Login with User Name and password

# Now Login github repository and create new repository.

## put repository name as pkg-contact or other as your choice.
## put description same as composer.json description of creacted package or your choice.
## set repository as private

# Create README.md file in package/contact folder of created folder so that user can understand this package is for. After that put some description and details. 

```
# Contact Us Form Package.

## This will store contact data to database.
```

Note: You can write readme more content , Its depednds on you.

# Now push package to repository.
Note: push only contact package files of package folder , Not complete main project

Run some command to push files to repository as :

```
git init

git status

git add .

git commit -m "Initial commit"

git remote add origin https://github.com/mmanjeet/pkg-contact.git

git push -u origin master

```

## After Pushing package on remote repository, I need to copy repository url from browser address bar as

```
https://github.com/mmanjeet/pkg-contact
```
## After copy the url of repository, I need to follow given instructions.

```
1-> Go to packagist (Login must be required)
2-> Click Submit Menu
3-> Paste the copied repository URL and click check button
Note: If your package vendor name is existed on packagist plateform, You need to change chnage vendor name of package and again push on remote repository.

as :


{
    "name": "CyberZet/contact",
    "description": "This will store contact data to database.",
    "type": "library",
    "license": "MIT",
    "autoload": {
        "psr-4": {
            "Trick\\Contact\\": "src/"
        }
    },
    "minimum-stability": "dev",
    "require": {}
}


Note:  If no warning will return, Submit it
```


# Now You will find a message on packagist as : 

```
This package is not auto-updated. Please set up the GitHub Hook for Packagist so that it gets updated whenever you push!
```
It means you are unable to automatic update if you changes in your git repository. 

## To Fix it follow given instructions.

```
1-> Go to Packagist
2-> Hover on User Name in right top bar.
3-> click setting link.
4-> click on connect github account button under "Using GitHub" section. Now warning message will remove from your package.

```

## Still now you will be unable to use this package beacuse It's minimum stability is "dev".

## To Fix It, I need to follow given instructions.

1-> Go to package/contact/composer.json file, and add extra attribute for managing providers.

```
"extra": {
        "laravel": {
            "providers": [
                "Trick\\Contact\\ContactServiceProvider"
            ]
        }
    }
```
and Now Need to update composer as:

```
composer update
```

2-> After that check git status of package.

```
git status
```

3->  Before commiting, I need to tag for allowing vesrion of package.

```
git tag v1.0.0
```

4->  After that commit as

```
git status
git add .
git commit -m "First vesrion release"
```

5-> After that I need to push

Note: In this time , I need to push with tag because I have currently added a new tag with v1.0.0.

```
git push --tag
```

Now You can see ther tag,version and relase on packagist and github.

6-> Now you can use this package, Its working fine.

Note: You need to add ContactServiceProvide in config/app.php file.


## Finally I can set my packge minimum stabilty as your requirement.