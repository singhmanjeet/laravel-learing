# Event AND Listener

## Event
Events represent significant actions or occurrences in your application. An event is triggered when an action occurs.


## Listener

Listener perform the certain task when that event is triggered.


Note: If we use listeners, I will need to register Event with Listener in EventServiceProvider.php
```
  protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SendEvent::class => [  // Event
            SendFired::class   // Listener
        ]
    ];
```


## Types of Event

# Built-In Events: 
These are events that are included by default within Laravel or its various components.

## 1 Authentication Events (Illuminate\Auth\Events):

Illuminate\Auth\Events\Login: Triggered when a user logs in.

Illuminate\Auth\Events\Logout: Triggered when a user logs out.
## 2 Cache Events (Illuminate\Cache\Events):

Illuminate\Cache\Events\CacheHit: Triggered when an item is found in the cache.

Illuminate\Cache\Events\CacheMissed: Triggered when an item is not found in the cache.

Illuminate\Cache\Events\KeyForgotten: Triggered when a cache key is forgotten.

Illuminate\Cache\Events\KeyWritten: Triggered when a cache key is written.

## 3 Database Events (Illuminate\Database\Events):

Illuminate\Database\Events\QueryExecuted: Triggered after a database query has been executed.

## 4 Queue Events (Illuminate\Queue\Events):

Illuminate\Queue\Events\JobProcessed: Triggered after a job has been successfully processed.

Illuminate\Queue\Events\JobProcessing: Triggered before a job is processed.

Illuminate\Queue\Events\JobFailed: Triggered when a job processing has failed.

## 5 Routing Events (Illuminate\Routing\Events):

Illuminate\Routing\Events\RouteMatched: Triggered when a route has been matched during the routing process.

Illuminate\Routing\Events\RouteExecuting: Triggered before a route is executed.

## 6 Filesystem Events (Illuminate\Filesystem\Events):

Illuminate\Filesystem\Events\FileDownloading: Triggered before a file is downloaded.

## 7 Model Events (Eloquent ORM):

Events like creating, created, updating, updated, deleting, deleted, etc., associated with Eloquent models.



# User Defined Events:
These are events that you create in your application to represent specific occurrences or actions.

Like: 
OrderShipped, InvoicePaid etc.

