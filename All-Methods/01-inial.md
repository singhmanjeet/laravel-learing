# Initial Methods

## all()
The all() method retrieves all records from the associated database table for the Eloquent model.
The all() method automatically eager loads all relationships defined on the model. so there is no need to use eager loading manually. 

```
User::all();
```

## get()

The get() method is used to execute a query builder instance and retrieve the resulting records.
The get() method does not automatically eager load relationships. If you want to load relationships, you need to specify them using the with() method.


```
User::get();
```


## find($id)

It will retrieve the record with the specified ID from the database.
```
$id=1;
User::find($id);
```

## findMany($ids)

It will retrieve the records with the specified IDs from the database.

```
$ids = [1, 2, 3];
User::findMany($ids);
```

## findOrNew($id)
This can be useful when you want to retrieve a record if it exists or create a new one if it doesn't, based on a given primary key.
```
$id = 1;
$modelInstance = User::findOrNew($id);
if ($modelInstance->exists) {
    // Record found
    echo "Existing Record - ID: " . $modelInstance->id . "\n";
} else {
    // Record not found, but a new record is created
    echo "New Record with ID: " . $modelInstance->id . "\n";
}
```


## 
It will attempt to find a record with the specified ID. If the record is found, it returns the existing instance; otherwise, it throws a ModelNotFoundException. You can catch this exception to handle the case when the record is not found.

```
$id = 1;
try {
    $modelInstance = User::findOrFail($id);
    // Record found
    echo "Existing Record - ID: " . $modelInstance->id . "\n";
} catch (\Illuminate\Database\Eloquent\ModelNotFoundException $exception) {
    // Record not found
    echo "Record not found for ID: " . $id . "\n";
}
```