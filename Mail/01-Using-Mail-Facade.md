# Using Mail Facade

## Step 1
create project in laravel

```
composer create-project laravel/laravel simple-mail
```
Go to your project as simple-mail

```
cd simple-mail
```


## Step 2

Install PDF package if you want to send any pdf attachment other wise you can skip it.


```
composer require barryvdh/laravel-dompdf
```

## Step 3

Configer SMTP in .env file

```
MAIL_MAILER=smtp
MAIL_HOST=mailpit
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS="hello@example.com"
MAIL_FROM_NAME="${APP_NAME}"
```

## Step 4

Create Controller

```
php artisan make:controller BasicMail
```

## Step 5

Create Route to send mail

```
use App\Http\Controllers\BasicMail;
Route::get('send-mail', [BasicMail::class, 'sendMail']);
```

## Step 6

create View in resources/views
name: basic-mail.blade.php you can change this file name.


```
use PDF;
use Mail;

class BasicMail extends Controller
{
    public function sendMail()
    {
        $data = ['title' => "This is Testing Title", "content" => "This is my testing content."];
        $pdf = PDF::loadView('mails.testmail', $data);

        Mail::send('mails.testmail', $data, function ($message) use ($data, $pdf) {
            $message->to("ms8776776@gmail.com")->subject("Testing mail with attachment")->attachData($pdf->output(), "test.pdf");
        });
    }
}
```

# Step 7 

create method in BasicMail Controller
## (a) with pdf attachment

```
use PDF;
use Mail;

class BasicMail extends Controller
{
    public function sendMail()
    {
        $data = ['title' => "This is Testing Title", "content" => "This is my testing content."];
        $pdf = PDF::loadView('mails.testmail', $data);

        Mail::send('mails.testmail', $data, function ($message) use ($data, $pdf) {
            $message->to("ms8776776@gmail.com")->subject("Testing mail with attachment")->attachData($pdf->output(), "test.pdf");
        });
    }
}
```


## (b) without attachment

```
use Mail;

class BasicMail extends Controller
{
    public function sendMail()
    {
        $data = ['title' => "This is Testing Title", "content" => "This is my testing content."];
        Mail::send('basic-mail', $data, function ($message) use ($data) {
            $message->to("ms8776776@gmail.com")->subject("Testing mail with attachment");
        });
    }
}
```
