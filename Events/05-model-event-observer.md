# Model Event Observer
Observer will create a observer class for particular model so that you can handle all events in that single observer class.

## create observer with model
This command will place the new observer in your App/Observers directory. 

```
php artisan make:observer CrudObserver --model=Crud
```
## Binding Observer with Model in App\Providers\AppServiceProvider.php

```
public function boot(): void
    {
        Crud::observe(CrudObserver::class);  // Binding CrudObserver with Crud Model
    }

```

## creating and created event setup.

```
namespace App\Observers;

use App\Models\Crud;
use Illuminate\Support\Facades\Log;

class CrudObserver
{
    /**
     * Handle the Crud "creating" event.
     */
    public function creating(Crud $crud): void
    {
        Log::info('Creating Event Fired' . json_encode($crud));  // when fire creating event
    }
    /**
     * Handle the Crud "created" event.
     */
    public function created(Crud $crud): void
    {
        Log::info('Created Event Fired' . json_encode($crud));   // when created event fired
    }

    /**
     * Handle the Crud "updated" event.
     */
    public function updated(Crud $crud): void
    {
        //
    }

    /**
     * Handle the Crud "deleted" event.
     */
    public function deleted(Crud $crud): void
    {
        //
    }

    /**
     * Handle the Crud "restored" event.
     */
    public function restored(Crud $crud): void
    {
        //
    }

    /**
     * Handle the Crud "force deleted" event.
     */
    public function forceDeleted(Crud $crud): void
    {
        //
    }
}

```